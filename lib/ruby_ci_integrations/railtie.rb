# frozen_string_literal: true

require 'ruby_ci_integrations'
require 'rails'

module RubyCiIntegrations
  # Allowing the gem to integrate with rails
  class Railtie < Rails::Railtie
    railtie_name :ruby_ci_integrations

    rake_tasks do
      path = File.expand_path(__dir__)
      Dir.glob("#{path}/tasks/**/*.rake").each { |f| load f }
    end
  end
end
