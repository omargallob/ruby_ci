# frozen_string_literal: true

# rubocop: disable Metrics/BlockLength
namespace :ci do
  namespace :gitlab do
    desc 'Gitlab install entrypoint'
    task :install do
      # Check if .gitlab-ci.yml exists

      filename = '.gitlab-ci.yml'
      # p File.file?(filename)
      spec = Gem::Specification.find_by_name('ruby_ci_integrations')
      gem_root = spec.gem_dir

      if File.file?("#{Dir.pwd}/#{filename}")
        cli = HighLine.new
        cli.choose do |menu|
          menu.prompt = 'You wish to overwrite(1) or abort(2)?'
          menu.choices(:overwrite) do
            cli.say('Perfect! overwriting')
            FileUtils.cp "#{gem_root}/.gitlab-ci.yml", Dir.pwd
            p 'gitlab-ci file copied from gem'
          end
          menu.choice(:abort) do
            cli.say('Your are leaving the guided walkthrough')
            cli.say('.gitlab-ci.yml is untouched')
          end
        end
      else
        p 'no gitlab-ci file'
        # copy the gitlab-ci to the root of the app
        FileUtils.cp "#{gem_root}/.gitlab-ci.yml", Dir.pwd
        p 'gitlab-ci file copied from gem'
      end
    end
  end
end
# rubocop: enable Metrics/BlockLength
