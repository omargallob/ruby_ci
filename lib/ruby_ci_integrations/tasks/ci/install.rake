# frozen_string_literal: true

require 'highline'

namespace :ci do
  desc 'Install entrypoint'
  task :install do
    cli = HighLine.new
    cli.choose do |menu|
      menu.prompt = 'Are you using gitlab or github? '
      menu.choice(:gitlab) do
        cli.say('Good choice! executing ci:gitlab:install')
        Rake::Task['ci:gitlab:install'].execute
      end
      menu.choices(:github) { cli.say('Not ready yet...') }
      menu.default = :gitlab
    end
  end
end
