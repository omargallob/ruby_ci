# frozen_string_literal: true

require_relative 'ruby_ci_integrations/version'

# RubyCis is the main module
module RubyCiIntegrations
  class Error < StandardError; end

  require 'ruby_ci_integrations/railtie' if defined?(Rails)
end
