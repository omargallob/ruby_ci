# frozen_string_literal: true

require_relative 'lib/ruby_ci_integrations/version'

Gem::Specification.new do |spec|
  spec.name = 'ruby_ci_integrations'
  spec.version = RubyCiIntegrations::VERSION
  spec.authors = ['Omar Gallo']
  spec.email = ['omargallob@gmail.com']

  spec.summary = 'Simple gem to add ci files for ruby project'
  spec.description = 'Gitlab or Github pipelines for a ruby project'
  spec.homepage = 'https://gitlab.com/omargallob/ruby_ci'
  spec.license = 'MIT'
  spec.required_ruby_version = '>= 2.6.0'

  spec.metadata['allowed_push_host'] = 'https://rubygems.org'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/omargallob/ruby_ci'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/omargallob/ruby_ci/CHANGELOG.md'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = 'exe'
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  # Uncomment to register a new dependency of your gem
  spec.add_dependency 'highline', '~> 2.0.3'
  spec.add_dependency 'rake', '~> 13.0'
  spec.add_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'bundle-audit'
  spec.add_development_dependency 'dotenv'
  spec.add_development_dependency 'rails', '~> 7.0.3'
  spec.add_development_dependency 'rails-dummy'
  spec.add_development_dependency 'rspec-rails', '~> 4.0'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'simplecov'
  spec.add_runtime_dependency 'sqlite3'
  # For more information and examples about making a new gem, checkout our
  # guide at: https://bundler.io/guides/creating_gem.html
end
